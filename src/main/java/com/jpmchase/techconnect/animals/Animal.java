package com.jpmchase.techconnect.animals;

import com.jpmchase.techconnect.Food;

/**
 * @author leon on 4/19/18.
 */
public interface Animal {
    String speak();
    Integer getNumberOfMealsEaten();
    Integer getId();
    void eat(Food food);

}
